extends OptionButton

var button_id_dictionary : Dictionary
onready var root : Spatial = $"../../../../../"

func _ready():
	add_item("-1", 0)
	for i in range(1, Items.item_ids.size() + 1):
		var id : int = Items.item_ids[i-1]
		button_id_dictionary[i] = Items.item_ids[i-1]
		add_item(String(id), i)


func _on_OptionButton_item_selected(ID):
	if ID > 0:
		root.spawn_id(button_id_dictionary[ID])
