extends Camera

export(NodePath) var camera_path : NodePath
onready var camera : Camera = get_node(camera_path)

func _ready():
	pass # Replace with function body.

func _process(delta):
	global_transform = camera.global_transform
	size = camera.size
