extends Control

onready var action_text : Label = $"TextAnchor/ActionText"
export(NodePath) var first_focus_path
onready var first_focus : Control = get_node(first_focus_path)
onready var highlight : Control = $Inventory/NinePatchRect
onready var player : KinematicBody = get_parent().get_parent()

var spaces : Array = []
onready var active : Control = $"Inventory/GridContainer/1"

func _ready():
	first_focus.grab_focus()
	clear_action_text()
	for i in range(Constants.INVENTORY_SIZE):
		var node : Control = get_node("Inventory/GridContainer/%d" % (i + 1))
		node.connect("focus_entered", node, "_on_set_active")
		spaces.append(node)
	


func _process(delta):
	var y : float = (Input.get_action_strength("circle_menu_up") - Input.get_action_strength("circle_menu_down"))
	var x : float = (Input.get_action_strength("circle_menu_right") - Input.get_action_strength("circle_menu_left"))
	var angle : float = Vector2(x, y).normalized().angle()
	if abs(x + y) > 0:
		#print(angle)
		var double_angle : float = angle + PI
		var ranges : int = 7
		var area : float = (2 * PI) / ranges
		var which_range : int = clamp(floor(double_angle / area), 0, ranges - 1)
		print(angle, ' ', which_range)
	if Input.is_action_just_pressed("drop"):
		Items.instantiate_ground_object(player.drop(int(active.name) - 1), player.get_parent(), player.translation)


func display_action_text(text : String):
	action_text.text = text

func clear_action_text():
	display_action_text("")

func update_inventory(inv : Array):
	for i in range(inv.size(), Constants.INVENTORY_SIZE):
		var tex : TextureRect = spaces[i].get_node("Icon")
		tex.texture = null

	for i in range(inv.size()):
		var tex : TextureRect = spaces[i].get_node("Icon")
		tex.texture = load(Items.items[inv[i]].icon)

func set_active(node : Control):
	highlight.rect_global_position = node.rect_global_position
	active = node