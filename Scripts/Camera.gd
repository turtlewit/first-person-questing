extends Camera

const RIGHT : Vector3 = Vector3(1, 0, 0)

export(float) var speed : float = 5.0
export(float) var max_degrees : float = 70
export(float) var mouse_sensitivity_y : float = 1.0

func _ready() -> void:
	pass 

func _process(delta: float) -> void:
	pass

func _physics_process(delta: float) -> void:
	do_rotation(delta)

func do_rotation(delta: float) -> void:
	var angle : float = (Input.get_action_strength('camera_up') - Input.get_action_strength('camera_down')) * speed * delta
	turn(angle)

func turn(angle: float) -> void:
	var old_transform : Transform = transform
	rotate_x(angle)
	if rotation_degrees.x > max_degrees:
		transform = old_transform
		rotation_degrees.x = max_degrees
	elif rotation_degrees.x < -max_degrees:
		transform = old_transform
		rotation_degrees.x = -max_degrees

func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		var motion : InputEventMouseMotion = event
		turn(-motion.relative.y * mouse_sensitivity_y)