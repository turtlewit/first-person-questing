extends Camera

onready var parent : Spatial = $"../"
export(float) var sensitivity : float = 0.01
export(float) var scroll_factor : float = 0.01

func _ready():
	pass 

func _process(delta):
	pass

func _input(event):
	if event is InputEventMouseMotion:
		if Input.is_action_pressed("icon_generator_pan"):
			parent.rotation.y += -event.relative.x * sensitivity
			parent.rotation.x += -event.relative.y * sensitivity
	elif event is InputEventMouseButton:
		if event.button_index == BUTTON_WHEEL_DOWN:
			size += scroll_factor
		elif event.button_index == BUTTON_WHEEL_UP:
			size -= scroll_factor