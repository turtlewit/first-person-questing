extends Area

class_name GroundItem

var actions : Array = [
	{
		action_name = "Pick up",
		function = funcref(self, "_pick_up")
	}
]

export(int) var id : int

func _pick_up(player : Node):
    if player.add_inventory_item(id):
        queue_free()