extends KinematicBody

class_name Player

const GRAVITY : Vector3 = Vector3(0, -9.8, 0)
const UP : Vector3 = Vector3(0, 1, 0)
const SNAP_GROUND : Vector3 = Vector3(0, -2, 0)
const SNAP_AIR : Vector3 = Vector3(0, 0, 0)

enum MovementState {GROUND, AIR, NONE}
enum ControlState {NORMAL, MENU}

export(float) var speed : float = 5.0
export(float) var rotation_speed : float = 5.0
export(float) var mouse_sensitivity_x : float = 1.0
export(MovementState) var state : int = MovementState.AIR
export(ControlState) var control_state : int = ControlState.NORMAL
export(Vector3) var linear_velocity : Vector3 = Vector3(0, 0, 0)

var ground_bodies : int = 0
var looking_at : Node
onready var entity_raycast : RayCast = $"CameraOrigin/Camera/EntityDetectionRay"
onready var ui : Control = $"CanvasLayer/UI"

# Mechanics
class Inventory:
	var _inventory : Array = []
	func add_item(id : int) -> bool:
		# Returns false if cannot add
		if _inventory.size() <= Constants.INVENTORY_SIZE:
			_inventory.append(id)
			return true 
		return false 
	
	func remove_item(id : int) -> bool:
		# Returns false if cannot remove
		if _inventory.has(id):
			_inventory.erase(id)
			return true 
		return false
	
	func drop_from_slot(slot : int) -> int:
		if slot + 1 > _inventory.size():
			return -1
		var id : int = _inventory[slot]
		_inventory.remove(slot)
		return id
	
	func get_inventory_array() -> Array:
		return _inventory.duplicate()

var inventory : Inventory = Inventory.new()

func _ready() -> void:
	Input.set_mouse_mode(2)

func _process(delta: float) -> void:
	detect_raycast()
	if Input.is_action_just_pressed("action") and looking_at:
		looking_at.actions[0].function.call_func(self)
		ui.update_inventory(inventory.get_inventory_array())

func _physics_process(delta: float) -> void:
	move(delta)

func move(delta: float) -> void:
	var snap : Vector3
	turn(delta)
	match state:
		MovementState.GROUND:
			ground_move(delta)
			linear_velocity = move_and_slide_with_snap(linear_velocity, SNAP_GROUND, UP)
		MovementState.AIR:
			air_move(delta)
			linear_velocity = move_and_slide_with_snap(linear_velocity, SNAP_AIR, UP)
	if is_on_floor():
		state = MovementState.GROUND
	else:
		state = MovementState.AIR

func ground_move(delta: float) -> Vector3:
	# z- is forward
	var forward : float = (Input.get_action_strength('move_backward') - Input.get_action_strength('move_forward')) * speed
	# x+ is right
	var right : float = (Input.get_action_strength('move_right') - Input.get_action_strength('move_left')) * speed

	var movement : Vector3 = global_transform.basis.z * forward + global_transform.basis.x * right

	linear_velocity = Vector3(movement.x, linear_velocity.y, movement.z)
	return SNAP_GROUND

func air_move(delta: float) -> Vector3:
	linear_velocity += GRAVITY * delta
	return SNAP_AIR

func turn(delta: float) -> void:
	var angle : float = (Input.get_action_strength('camera_left') - Input.get_action_strength('camera_right')) * rotation_speed

	rotate_y(angle * delta)

func on_ground() -> bool:
	return ground_bodies > 0

func detect_raycast() -> void:
	var col : Object = entity_raycast.get_collider()
	if col == null:
		if looking_at != null:
			look_away()
		return 
	if not 'Ground Item' in col.get_groups():
		look_away()
	if 'Ground Item' in col.get_groups() and col != looking_at:
		display_item(col)

func look_away() -> void:
	looking_at = null
	ui.clear_action_text()

func drop(slot: int) -> int:
	var id : int = inventory.drop_from_slot(slot)
	ui.update_inventory(inventory._inventory)
	return id

func display_item(col: GroundItem) -> void:
	looking_at = col
	if col.id in Items.item_ids:
		var text : String = col.actions[0].action_name + ' ' + Items.items[col.id].name
		ui.display_action_text(text)

func add_inventory_item(item_id: int) -> bool:
	# Returns false if cannot pick up
	return inventory.add_item(item_id)

func _ground_entered(body: Node) -> void:
	if 'Player' in body.get_groups():
		return 
	ground_bodies += 1

func _ground_exited(body: Node) -> void:
	if 'Player' in body.get_groups():
		return 
	ground_bodies -= 1

func _on_focus_entered() -> void:
	print("Entered")

func _on_focus_exited() -> void:
	print("Exited")

func _input(event: InputEvent):
	if event is InputEventMouseMotion:
		var motion : InputEventMouseMotion = event
		rotate_y(-motion.relative.x * mouse_sensitivity_x)