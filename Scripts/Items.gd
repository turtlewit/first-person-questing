extends Node 

# Slow, don't iterate over
const items : Dictionary = {
    0 : {
        name = "null"
    },
    1000 : {
        name = "wood",
        ground_item = "res://prefabs/ground_items/resources/wood/wood.tscn",
        icon = "res://icons/100.png"
    },
    100 : {
        name = "axe"
    }
}

var item_ids : PoolIntArray = PoolIntArray(items.keys())

func instantiate_ground_object(id: int, parent_node : Node, position: Vector3 = Vector3(0, 0, 0)) -> GroundItem:
	if not id in item_ids:
		return null
	var ground_item_path : String = items[id].get('ground_item')
	if ground_item_path:
		var prefab : PackedScene = load(ground_item_path)
		var ground_item : GroundItem = prefab.instance()
		ground_item.translation = position
		parent_node.add_child(ground_item)
		return ground_item
	return null