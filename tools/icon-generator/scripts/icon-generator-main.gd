extends Spatial

var spawned_item : GroundItem
onready var viewport : Viewport = find_node("Viewport")

func _ready():
	pass 

func _process(delta):
	pass

func spawn_id(id: int) -> void:
	if spawned_item:
		spawned_item.queue_free()
	spawned_item = Items.instantiate_ground_object(id, self)


func _on_Render_pressed():
	if not spawned_item:
		return
	var tex : ViewportTexture = viewport.get_texture()
	var path : String = Constants.ICON_PATH + '/%d.png' % spawned_item.id
	var img : Image = tex.get_data()
	img.flip_y()
	img.save_png(path)


func _on_HSlider_value_changed(value):
	if spawned_item:
		spawned_item.translation.y = value
